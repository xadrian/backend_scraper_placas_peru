﻿require('dotenv').config();
var main = require('./main.js');

var event = {
	"request": {
		idRequest: 42,
		inputData: (process.argv[2] || 'B7E164')
	},
	"groupName": "op32"
};

main.handler(event);