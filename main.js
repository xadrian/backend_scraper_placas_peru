﻿var bck = require('backend-lib-nodejs');
var cheerio = require("cheerio");
var stripTags = require('strip-tags');

const BACKUP_IS_NECESSARY = process.env.BACKUP_IS_NECESSARY || false;
const BASE_URL = "https://www.placas.pe/Public/CheckPlateStatus.aspx";

exports.handler = function (event, context, callback) {
	
	bck.processOp(event, callback, function (fulfill, reject, placas) {
		var patente = placas[0].patente;
	    bck.httpClient.get(BASE_URL,
	        function(inError, inResponse, inBody) {
				if (inError) {
					return reject(inError);
				}else{
					var $ = cheerio.load(inBody);
					var data = {
						ctl00$MainContent$wibSearch:"Buscar",
						ctl00$MainContent$txtimgcode:"",
						ctl00$MainContent$wdpDateFin$meFecha_ClientState:"",
						ctl00$MainContent$wdpDateFin$TxtFecha:$('#MainContent_wdpDateIni_TxtFecha').val(),
						ctl00$MainContent$wdpDateIni$meFecha_ClientState:"",
						ctl00$MainContent$wdpDateIni$TxtFecha:$('#MainContent_wdpDateFin_TxtFecha').val(),
						ctl00$MainContent$txtRequirementPlateId:"",
						ctl00$MainContent$txtPlateNumber:patente,
						ctl00$MainContent$wddTypePlate:"0",
						__EVENTVALIDATION:$('#__EVENTVALIDATION').val(),
						__VIEWSTATEGENERATOR:$('#__VIEWSTATEGENERATOR').val(),
						__VIEWSTATE:$('#__VIEWSTATE').val(),
						__LASTFOCUS:"",
						__EVENTARGUMENT:"",
						__EVENTTARGET:""
					}
					descubreCaptcha().then(function(captcha){
						data.ctl00$MainContent$txtimgcode = captcha.solution;
						bck.httpClient.post({
							url:BASE_URL,
							form: data
						},function(seError,seResponse, seBody){
							if(seError){
								return reject(seError);
							}else{
								processData(patente,seBody,fulfill,reject);
							}
						})

					}).catch(function(e){
						return reject(e);
					})
				}
			}
		);
	})
};
function descubreCaptcha(){
	return new Promise(function(resolve,reject){
		var captchaImg = 'https://www.placas.pe/UserControls/FrmCaptcha.aspx';
		bck.httpClient.get({
				url: captchaImg,
				encoding: "binary"
			},
			function (imError, imResponse, imBody) {
				var buffer = new Buffer(imBody, "binary");
				if(imError){
					return reject(imError);
				}else{
					bck.captcha.read(buffer)
						.then(resolve).catch(reject);
				}
			}
		)

	});
}
function processData(patente, responseBody, fulfill, reject) {
	try {
		var $ = cheerio.load(responseBody);
		var informe = { informacionMecanica:{} };
		var data = {};
		if($('#MainContent_tagDataVehicle').length > 0){
			$('#MainContent_tagDataVehicle tr').each(function(index, tr) {

				var label = $(tr).children().first().text();
				var value = $(tr).children().last().text();

				label = slug(clean(label));
				value = clean(value);
				data[label] = value;

			});
		}

		if(data.nro_serie){
			informe.informacionMecanica = {
				"vin":data.nro_serie,
				"modelo":data.modelo,
				"marca":data.marca
			};
		}

	    $("head").prepend("<base href='https://www.placas.pe'>");

	    var codeHTML = $.html();
	    codeHTML =  stripTags(codeHTML, ['script']);

	    bck.backupHtml(informe, codeHTML, 'Placas_PERU', 'Placas_PERU_WEB' + patente, 'Consulta_Movimiento_Placa')
	              .then(function(ok) {
	                  fulfill(informe);
	              })
	              .catch(function(error) {
	                  if (BACKUP_IS_NECESSARY) {
	                      reject(error);
	                  } else {
	                      console.log(error);
	                      fulfill(informe);
	                  }
	              });

	} catch (error) {
		reject(error);
	}
}

function clean(string){
	return string.toLowerCase().replace(/(\r|\n|:)/g,'').replace(/\s+-/,'').trim();
}
function slug(str){
	return str.replace(/\./,'').replace(/\s/g,'_');
}